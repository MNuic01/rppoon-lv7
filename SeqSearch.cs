﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    class SeqSearch : SearchStrategy
    {
        public override int Search(double[] array, double n)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                if (array[i] == n)
                    return i;
            }
            return -1;
        }
    }
}
