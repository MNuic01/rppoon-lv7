﻿using System;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence numberSequence = new NumberSequence(new double[] { 7, 2, 6, 8, 3, 1});
            NumberSequence seqbubble = numberSequence;
            NumberSequence seqcomb = numberSequence;

            numberSequence.SetSortStrategy(new SequentialSort());
            seqbubble.SetSortStrategy(new BubbleSort());
            seqcomb.SetSortStrategy(new CombSort());

            numberSequence.Sort();
            seqbubble.Sort();
            seqcomb.Sort();
            Console.WriteLine("sorting sequentially:");
            Console.WriteLine(numberSequence.ToString());

            Console.WriteLine("Sorting using Bubble sort:");
            Console.WriteLine(seqbubble.ToString());

            Console.WriteLine("Sorting using Comb sort:");
            Console.WriteLine(seqcomb.ToString());



            numberSequence.SetSearchStrategy(new SeqSearch());
            int index = numberSequence.Search(7);
            if (index == -1)
            {
                Console.WriteLine("Array does not contain this number");
            }
            else
            {
                Console.WriteLine("The number is placed on " + index + ". index in an array");
            }
        }
    }
}
